package com.runemate.game.api.osrs.local;

import com.runemate.game.api.hybrid.local.*;
import com.runemate.game.api.hybrid.local.hud.*;
import com.runemate.game.api.hybrid.local.hud.interfaces.*;
import com.runemate.game.api.hybrid.util.*;
import com.runemate.game.api.osrs.data.*;
import com.google.common.collect.*;
import java.util.*;
import lombok.experimental.*;

@UtilityClass
public class Quiver {

    private static final Set<Integer> DIZANAS_QUIVER_IDS = ImmutableSet.<Integer>builder()
        .addAll(ItemVariations.getVariations(ItemVariations.getBase(ItemID.DIZANAS_QUIVER)))
        .addAll(ItemVariations.getVariations(ItemVariations.getBase(ItemID.BLESSED_DIZANAS_QUIVER)))
        .addAll(ItemVariations.getVariations(ItemVariations.getBase(ItemID.DIZANAS_MAX_CAPE)))
        .build();

    /**
     * True if the player has a charged quiver equipped.
     */
    public static boolean isEquipped() {
        SpriteItem equipped = Equipment.getItemIn(Equipment.Slot.CAPE);
        return equipped != null && DIZANAS_QUIVER_IDS.contains(equipped.getId());
    }

    /**
     * True if the player has ammunition in the quiver slot.
     * More performant than {@code #getAmmunition() != null} but does not check if the quiver is actually equipped.
     */
    public static boolean hasAmmunition() {
        Varp varp = Varps.getAt(VarpID.QUIVER_AMMO_ITEM_ID.getId());
        return varp.getValue() != -1;
    }

    /**
     * The item in the quiver ammunition slot.
     */
    public static SpriteItem getAmmunition() {
        int id = Varps.getAt(VarpID.QUIVER_AMMO_ITEM_ID.getId()).getValue();
        if (id == -1) {
            return null;
        }

        int quantity = Varps.getAt(VarpID.QUIVER_AMMO_QUANTITY.getId()).getValue();
        if (quantity == 0) {
            return null;
        }

        return new SpriteItem(id, quantity, 0, SpriteItem.Origin.QUIVER);
    }

    /**
     * The component of the equipment slot containing the secondary ammunition.
     */
    public static InterfaceComponent getEquipmentSlotComponent() {
        return Interfaces.getAt(387, 28);
    }

    /**
     * The bounds of the equipment slot containing the secondary ammunition.
     */
    public static InteractableRectangle getEquipmentSlotBounds() {
        return Optional.ofNullable(getEquipmentSlotComponent())
            .map(InterfaceComponent::getBounds)
            .orElse(null);
    }
}
