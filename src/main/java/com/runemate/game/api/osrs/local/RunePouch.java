package com.runemate.game.api.osrs.local;

import static com.runemate.game.api.hybrid.local.VarbitID.*;

import com.runemate.game.api.hybrid.entities.definitions.*;
import com.runemate.game.api.hybrid.local.*;
import com.runemate.game.api.hybrid.input.*;
import com.runemate.game.api.hybrid.player_sense.PlayerSense;
import com.runemate.game.api.hybrid.local.hud.interfaces.*;
import com.runemate.game.api.osrs.local.hud.interfaces.*;
import com.runemate.game.api.script.*;
import java.awt.event.*;
import java.util.regex.*;
import java.util.*;
import lombok.*;
import org.jetbrains.annotations.*;

public class RunePouch {

    private static final Pattern RUNE_POUCH_PATTERN = Pattern.compile("^(Divine )?rune pouch( ?\\(l\\))?$", Pattern.CASE_INSENSITIVE);
    private static final int OPENED_POUCH_CONTAINER = 190;
    private static final int MAX_QUANTITY = 16000;

    private RunePouch() {
    }

    @Nullable
    public static Rune getRune(Slot slot) {
        return slot.getRune();
    }

    public static int getQuantity(Slot slot) {
        return slot.getQuantity();
    }

    public static int getQuantity(Rune rune) {
        for (final Slot slot : Slot.values()) {
            if (slot.getRune() == rune) {
                return slot.getQuantity();
            }
        }
        return 0;
    }

    public static boolean isEmpty() {
        return Arrays.stream(Slot.values()).allMatch(slot -> slot.getQuantity() == 0);
    }

    public static int getEmptySlots() {
        if(Slot.FOUR.isEmpty()) {
            SpriteItem pouchItem = getInventoryItem();
            if(pouchItem != null){
                ItemDefinition def = pouchItem.getDefinition();
                if(def != null && !def.getName().contains("Divine")){
                    return (int) Arrays.stream(Slot.values()).filter(slot -> slot.getQuantity() == 0 && slot != Slot.FOUR).count();
                }
            }
        }
        return (int) Arrays.stream(Slot.values()).filter(slot -> slot.getQuantity() == 0).count();
    }

    /**
     * @return true if Rune Pouch was completely emptied
     */
    public static boolean empty() {
        if(RunePouch.isOpen()) {
            return false;
        }
        if (RunePouch.isEmpty()) {
            return true;
        }
        SpriteItem pouch = getInventoryItem();
        return pouch != null
            && pouch.interact("Empty")
            && Execution.delayUntil(RunePouch::isEmpty, 700);
    }

    /**
     * Opens Rune Pouch store interface
     *
     * @return true if open action was successful
     */
    public static boolean open() {
        SpriteItem pouch = getInventoryItem();
        return pouch != null
            && pouch.interact("Open")
            && Execution.delayUntil(RunePouch::isOpen, 2000);
    }

    /**
     * @return true RunePouch interface is open
     */
    public static boolean isOpen() {
        return !Interfaces.newQuery()
            .containers(OPENED_POUCH_CONTAINER)
            .grandchildren(true)
            .types(InterfaceComponent.Type.LABEL)
            .texts(RUNE_POUCH_PATTERN)
            .visible()
            .results()
            .isEmpty();
    }

    /**
     * Uses PlayerSense to decide how to close the interface.
     *
     * @return true if close action was successful
     */
    public static boolean close() {
        return close(PlayerSense.getAsBoolean(PlayerSense.Key.CLOSE_BANK_WITH_ESCAPE));
    }

    /**
     * @param hotkey Whether the close should use the hotkey or not
     * @return true if close action was successful
     */
    public static boolean close(boolean hotkey) {
        if (!isOpen()) {
            return true;
        }
        if (hotkey && OptionsTab.AllSettings.INTERFACES_CLOSABLE_WITH_ESCAPE.isEnabled()) {
            return Keyboard.typeKey(KeyEvent.VK_ESCAPE)
                && Execution.delayWhile(RunePouch::isOpen, 2000, 2500);
        } else {
            InterfaceComponent button = Interfaces.newQuery()
                .containers(OPENED_POUCH_CONTAINER)
                .grandchildren(true)
                .types(InterfaceComponent.Type.SPRITE)
                .actions("Close")
                .visible()
                .results()
                .first();
            return button != null
                && button.interact("Close")
                && Execution.delayWhile(RunePouch::isOpen, 2000);
        }
    }

    /**
     * @param rune The type of rune to fill
     * @return true if able to fill rune to slot
     */
    private static boolean store(Rune rune) {
        int initialQuantity = RunePouch.getQuantity(rune);
        if (rune == null || initialQuantity == MAX_QUANTITY || (initialQuantity == 0 && RunePouch.getEmptySlots() == 0)) {
            return false;
        }
        Slot slot = Arrays.stream(Slot.values())
            .filter(slot1 -> slot1.getRune() == rune)
            .findFirst()
            .orElse(Arrays.stream(Slot.values())
                .filter(Slot::isEmpty)
                .findFirst()
                .orElse(null));
        if (slot == null) {
            return false;
        }
        if (RunePouch.isOpen()) {
            InterfaceComponent runePouchInventory = Interfaces.newQuery()
                .containers(190)
                .grandchildren(true)
                .grandchildren(28)
                .types(InterfaceComponent.Type.CONTAINER)
                .results()
                .first();
            if (runePouchInventory == null || !runePouchInventory.isVisible()) {
                return false;
            }
            InterfaceComponent item = runePouchInventory.getChildren()
                .stream()
                .filter(child -> child.getContainedItemId() == rune.getItemId())
                .findFirst()
                .orElse(null);
            return item != null
                && item.interact("Store-All")
                && Execution.delayUntil(() -> slot.getQuantity() > initialQuantity, 700);
        } else {
            SpriteItem pouch = RunePouch.getInventoryItem();
            SpriteItem runeItem = Inventory.newQuery().names(rune.getName()).results().first();
            SpriteItem selectedItem = Inventory.getSelectedItem();
            if(selectedItem != null && !selectedItem.equals(runeItem)) {
                if(!selectedItem.interact("Use") || !Execution.delayWhile(Inventory::isItemSelected, 2000)){
                    return false;
                }
            }
            return pouch != null
                && runeItem != null
                && (Inventory.isItemSelected() || runeItem.interact("Use"))
                && Execution.delayUntil(Inventory::isItemSelected, 700)
                && pouch.interact("Use", Pattern.compile("^" + rune.getName() + " -> " + RUNE_POUCH_PATTERN.toString().substring(1)))
                && Execution.delayUntil(() -> slot.getQuantity() > initialQuantity, 700);
        }
    }

    public static SpriteItem getInventoryItem() {
        return Inventory.newQuery().names(RUNE_POUCH_PATTERN).results().first();
    }

    @AllArgsConstructor
    public enum Slot {
        ONE(0, RUNE_POUCH_RUNE1, RUNE_POUCH_AMOUNT1),
        TWO(1, RUNE_POUCH_RUNE2, RUNE_POUCH_AMOUNT2),
        THREE(2, RUNE_POUCH_RUNE3, RUNE_POUCH_AMOUNT3),
        FOUR(3, RUNE_POUCH_RUNE4, RUNE_POUCH_AMOUNT4);

        private final int interfaceIndex;
        private final VarbitID type;
        private final VarbitID quantity;

        public Rune getRune() {
            final Varbit varbit = Varbits.load(type.getId());
            if (varbit != null) {
                final int value = varbit.getValue();
                for (final Rune rune : Rune.values()) {
                    if (rune.pouchType == value) {
                        return rune;
                    }
                }
            }
            return null;
        }

        public int getQuantity() {
            final Varbit varbit = Varbits.load(quantity.getId());
            return varbit == null ? 0 : varbit.getValue();
        }

        /**
         * @return true if slot is empty
         */
        public boolean isEmpty() {
            return getQuantity() == 0;
        }

        /**
         * @return true if able to empty slot
         */
        public boolean empty() {
            if (!RunePouch.isOpen()) {
                return false;
            }
            InterfaceComponent slotInterface = Interfaces.newQuery()
                .containers(190)
                .actions("Withdraw-All")
                .filter(interfaceComponent -> interfaceComponent.getIndex() == interfaceIndex)
                .results()
                .first();
            return slotInterface != null
                && slotInterface.interact("Withdraw-All")
                && Execution.delayUntil(() -> slotInterface.getActions().isEmpty(), 700);
        }

        @Override
        public String toString() {
            return "RunePouch.Slot." + name();
        }
    }
}
