package com.runemate.game.api.osrs;

import com.runemate.client.game.open.*;
import com.runemate.game.api.client.*;
import com.runemate.game.api.hybrid.*;
import java.util.*;
import org.apache.commons.lang3.*;

public final class OSRSClientPreferences {

    private OSRSClientPreferences() {
    }


    public static boolean areRoofsHidden() {
        return OpenClientPreferences.isHidingRoofs();
    }


    public static boolean isAudioDisabled() {
        return OpenClientPreferences.isAudioDisabled();
    }


    public static boolean isUsernameHidden() {
        return OpenClientPreferences.isUsernameHidden();
    }


    public static int getProtocolVersion() {
        return OpenClientPreferences.getProtocolVersion();
    }


    public static String getRememberedUsername() {
        return (String) OpenClientPreferences.getRememberedUsername();
    }


    public static Map<Integer, Integer> getRememberedTwoFactorTokens() {
        return OpenClientPreferences.getRememberedTwoFactorTokens();
    }

    private static int usernameToKey(CharSequence var0) {
        int var2 = var0.length();
        int var3 = 0;

        for (int var4 = 0; var4 < var2; ++var4) {
            var3 = (var3 << 5) - var3 + var0.charAt(var4);
        }

        return var3;
    }
}
