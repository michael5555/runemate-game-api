package com.runemate.game.api.osrs.local.hud.interfaces;

import com.runemate.client.game.open.*;
import com.runemate.game.api.hybrid.local.hud.interfaces.*;
import com.runemate.game.api.script.exceptions.*;
import com.runemate.rmi.*;
import java.util.*;
import java.util.function.*;
import java.util.stream.*;

public final class OSRSInterfaceComponent extends InterfaceComponent {

    public OSRSInterfaceComponent(final OpenInterfaceComponent component) {
        super(component);
    }

    @Override
    public InterfaceComponent getChild(int index) {
        if (isChildComponent() || Type.CONTAINER != getType()) {
            return null;
        }
        return Interfaces.getAt(getId() >> 16, getId() & 0xFFF, index);
    }

    @Override
    public InterfaceComponent getChild(Predicate<InterfaceComponent> predicate) {
        if (isChildComponent() || Type.CONTAINER != getType()) {
            return null;
        }
        if (predicate == null) {
            return getChildren().stream().findFirst().orElse(null);
        }
        return getChildren().stream().filter(predicate).findFirst().orElse(null);
    }

    @Override
    public List<InterfaceComponent> getChildren() {
        if (isChildComponent() || Type.CONTAINER != getType()) {
            return Collections.emptyList();
        }

        return OpenInterfaceComponentManager.getChildComponents(getId() >> 16, getId() & 0xFFF)
            .stream()
            .map(o -> (InterfaceComponent) new OSRSInterfaceComponent(o))
            .toList();
    }

    @Override
    public int getContainedItemQuantity() {
        return component().getItemQuantity();
    }

    @Override
    public int getProjectedEntityAnimationId() {
        return component().getAnimationId();
    }

    private String getButtonAction() {
        String name = component().getButtonAction();
        return name != null && !name.isEmpty() ? name : null;
    }

    @Override
    public List<String> getActions() {
        List<String> actions = super.getActions();
        if (actions.isEmpty()) {
            String buttonAction = getButtonAction();
            if (buttonAction != null && !buttonAction.equals("Ok")) {
                return Collections.singletonList(buttonAction);
            }
        }
        return actions;
    }
}
