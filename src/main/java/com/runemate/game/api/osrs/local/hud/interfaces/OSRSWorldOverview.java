package com.runemate.game.api.osrs.local.hud.interfaces;

import com.runemate.client.game.open.*;
import com.runemate.game.api.hybrid.cache.configs.*;
import com.runemate.game.api.hybrid.local.*;
import com.runemate.game.api.osrs.location.*;
import com.runemate.game.api.script.annotations.*;
import java.util.*;
import lombok.*;

public class OSRSWorldOverview implements WorldOverview {

    private static final int ENUM_WORLD_LOCATION = 4992;
    private static final int ENUM_LOCATION_FLAG = 1125;
    private static final int FLAG_US_WEST = 4936;
    private static final int FLAG_US_EAST = 4937;

    private final long uid;
    private final int id;
    private final int type;
    private final String activity;
    @Getter
    private final String address;
    private final int location;
    private final int population;

    public OSRSWorldOverview(long uid) {
        this.uid = uid;
        this.id = -1;
        this.type = -1;
        this.activity = null;
        this.address = null;
        this.location = -1;
        this.population = -1;
    }

    public OSRSWorldOverview(int id, int type, String activity, String address, int location, int population) {
        this.address = address;
        this.uid = -1;
        this.id = id;
        this.type = type;
        this.activity = activity;
        this.location = location;
        this.population = population;
    }

    @Override
    public int getPopulation() {
        return population;
    }

    @Override
    public WorldRegion getRegion() {
        return WorldRegion.valueOf(location);
    }

    @Override
    public WorldRegionQualifier getRegionQualifier() {
        WorldRegion region = getRegion();
        if (region != WorldRegion.AMERICA) {
            return null;
        }

        switch (getFlag()) {
            case FLAG_US_EAST:
                return WorldRegionQualifier.EAST;
            case FLAG_US_WEST:
                return WorldRegionQualifier.WEST;
            default:
                return null;
        }
    }

    @Override
    public int getId() {
        return uid != -1 ? OpenWorld.getNumber(uid) : id;
    }

    @Override
    public EnumSet<WorldType> getWorldTypes() {
        return WorldType.of(getMarkerBits());
    }

    @Override
    public String getActivity() {
        if (uid == -1) {
            return activity;
        }
        return OpenWorld.getActivity(uid);
    }

    private boolean hasSkillTotalRequirement() {
        return getWorldTypes().contains(WorldType.SKILL_TOTAL);
    }

    @Override
    public boolean isMembersOnly() {
        return getWorldTypes().contains(WorldType.MEMBERS);
    }

    @Override
    public boolean isSkillTotal500() {
        return hasSkillTotalRequirement() && Integer.parseInt(getActivity().replaceAll("\\D", "")) == 500;
    }

    @Override
    public boolean isSkillTotal750() {
        return hasSkillTotalRequirement() && Integer.parseInt(getActivity().replaceAll("\\D", "")) == 750;
    }

    @Override
    public boolean isSkillTotal1250() {
        return hasSkillTotalRequirement() && Integer.parseInt(getActivity().replaceAll("\\D", "")) == 1250;
    }

    @Override
    public boolean isSkillTotal1500() {
        return hasSkillTotalRequirement() && Integer.parseInt(getActivity().replaceAll("\\D", "")) == 1500;
    }

    @Override
    public boolean isSkillTotal1750() {
        return hasSkillTotalRequirement() && Integer.parseInt(getActivity().replaceAll("\\D", "")) == 1750;
    }

    @Override
    public boolean isSkillTotal2000() {
        return hasSkillTotalRequirement() && Integer.parseInt(getActivity().replaceAll("\\D", "")) == 2000;
    }

    @Override
    public boolean isSkillTotal2200() {
        return hasSkillTotalRequirement() && Integer.parseInt(getActivity().replaceAll("\\D", "")) == 2200;
    }

    public int getMarkerBits() {
        if (uid == -1) {
            return type;
        }
        return OpenWorld.getMarkerBits(uid);
    }

    private int getUnknownType() {
        int mask = getMarkerBits();
        if (isMembersOnly()) {
            mask ^= 0x1;
        }
        if (hasSkillTotalRequirement()) {
            mask ^= 0x80;
        }
        return mask;
    }

    @Override
    public String toString() {
        return "World " + getId() + " Overview";
    }

    private int getFlag() {
        try {
            var locationEnum = EnumDefinitions.load(ENUM_WORLD_LOCATION);
            var spriteEnum = EnumDefinitions.load(ENUM_LOCATION_FLAG);
            if (locationEnum == null || spriteEnum == null) {
                return -1;
            }

            var location = locationEnum.getInt(getId());
            return spriteEnum.getInt(location);
        } catch (Exception ignored) {
            return -1;
        }
    }
}
