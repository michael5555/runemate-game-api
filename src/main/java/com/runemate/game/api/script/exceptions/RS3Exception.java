package com.runemate.game.api.script.exceptions;

public class RS3Exception extends RuntimeException {
    public RS3Exception(String owner, String name) {
        super(owner + '#' + name + " is unsupported on RS3");
    }

    public RS3Exception() {
        super("An invoked method is unsupported on RS3");
    }
}
