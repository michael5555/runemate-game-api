package com.runemate.game.api.script.framework.listeners.events;

import com.runemate.game.api.hybrid.entities.*;
import com.runemate.game.api.hybrid.entities.definitions.*;
import com.runemate.game.api.hybrid.location.*;
import lombok.*;

@Value
public class NpcDefinitionChangedEvent implements EntityEvent {

    Npc npc;
    NpcDefinition previousDefinition;
    NpcDefinition currentDefinition;

    @Override
    public EntityType getEntityType() {
        return EntityType.NPC;
    }
}
