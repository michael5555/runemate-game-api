package com.runemate.game.api.hybrid.location.navigation.web.requirements;

import com.runemate.game.api.hybrid.local.hud.interfaces.*;
import java.io.*;
import lombok.*;

public class MoneyRequirement extends WebRequirement implements SerializableRequirement {
    private int amount;

    public MoneyRequirement(int amount) {
        this.amount = amount;
    }

    public MoneyRequirement(int protocol, ObjectInput stream) {
        super(protocol, stream);
    }

    public int getAmount() {
        return amount;
    }

    @Override
    public int getOpcode() {
        return 6;
    }

    @SneakyThrows(IOException.class)
    @Override
    public boolean serialize(ObjectOutput stream) {
        stream.writeInt(amount);
        return true;
    }

    @SneakyThrows(IOException.class)
    @Override
    public boolean deserialize(int protocol, ObjectInput stream) {
        amount = stream.readInt();
        return true;
    }

    @Override
    public boolean isMet0() {
        long total = 0;
        SpriteItem coins = Inventory.newQuery().names("Coins").results().first();
        if (coins != null) {
            total += coins.getQuantity();
            return total >= amount;
        }
        return false;
    }

    @Override
    public int hashCode() {
        return amount;
    }

    @Override
    public boolean equals(Object o) {
        return this == o ||
            o instanceof MoneyRequirement && ((MoneyRequirement) o).amount == this.amount;
    }

    @Override
    public String toString() {
        return "MoneyRequirement(amount=" + amount + ')';
    }
}
