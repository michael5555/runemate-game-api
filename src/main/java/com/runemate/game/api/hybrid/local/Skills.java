package com.runemate.game.api.hybrid.local;

import com.runemate.client.game.open.*;

/**
 * A utility for manipulating generic skill data.
 */
public final class Skills {
    private static final int[] STANDARD_EXPERIENCE_TABLE = new int[120];

    static {
        int accumulater = 0;
        for (int i = 1; i < 120; ++i) {
            accumulater = accumulater + (int) (i + 300.0D * StrictMath.pow(2.0D, i / 7.0D));
            STANDARD_EXPERIENCE_TABLE[i] = accumulater / 4;
        }
    }

    private Skills() {
    }

    public static Skill getByIndex(int index) {
        for (Skill skill : Skill.values()) {
            if (skill.getIndex() == index) {
                return skill;
            }
        }
        return null;
    }

    public static int[] getExperiences() {
        return OpenSkills.getCurrentExps();
    }

    public static int[] getBaseLevels() {
        return OpenSkills.getBaseLevels();
    }

    public static int[] getCurrentLevels() {
        return OpenSkills.getCurrentLevels();
    }

    /**
     * Gets the experience at a given level. If the data isn't loaded, returns -1
     */
    public static int getExperienceAt(int level) {
        if (level < 1) {
            return 0;
        } else {
            if (level > STANDARD_EXPERIENCE_TABLE.length) {
                level = STANDARD_EXPERIENCE_TABLE.length;
            }
            return STANDARD_EXPERIENCE_TABLE[level - 1];
        }
    }

    public static int getLevelAtExperience(Skill skill, int experience) {
        int[] table = STANDARD_EXPERIENCE_TABLE;
        int level = 1;
        for (int i = 1; i < table.length; i++) {
            int experienceAtLevel = table[i];
            if (experience < experienceAtLevel) {
                level = i;
                break;
            }
        }
        return Math.min(level, skill.getMaxLevel());
    }
}
