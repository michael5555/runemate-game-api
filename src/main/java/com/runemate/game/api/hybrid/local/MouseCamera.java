package com.runemate.game.api.hybrid.local;

import com.runemate.game.api.hybrid.*;
import com.runemate.game.api.hybrid.entities.*;
import com.runemate.game.api.hybrid.entities.details.*;
import com.runemate.game.api.hybrid.input.*;
import com.runemate.game.api.hybrid.local.hud.*;
import com.runemate.game.api.hybrid.location.Area;
import com.runemate.game.api.hybrid.location.*;
import com.runemate.game.api.hybrid.player_sense.*;
import com.runemate.game.api.hybrid.projection.*;
import com.runemate.game.api.hybrid.region.*;
import com.runemate.game.api.hybrid.util.*;
import com.runemate.game.api.hybrid.util.calculations.Random;
import com.runemate.game.api.hybrid.util.calculations.*;
import com.runemate.game.api.hybrid.util.collections.*;
import com.runemate.game.api.osrs.local.hud.interfaces.*;
import com.runemate.game.api.script.*;
import com.runemate.game.api.script.framework.*;
import com.google.common.cache.*;
import java.awt.*;
import java.awt.geom.*;
import java.util.*;
import java.util.concurrent.*;
import java.util.function.*;
import lombok.*;
import lombok.experimental.*;
import lombok.extern.log4j.*;
import org.jetbrains.annotations.*;

@Log4j2(topic = "MouseCamera")
@UtilityClass
public class MouseCamera {

    private static final double YAW_CONVERSION = 360d / 1024d;
    private static final double PITCH_CONVERSION = 1d / 128d;
    private static final Cache<AbstractBot, ForkJoinTask<?>> CONCURRENT_TASKS = CacheBuilder.newBuilder()
        .expireAfterAccess(30, TimeUnit.SECONDS)
        .build();

    public boolean concurrentlyTurnTo(Locatable end) {
        if (end == null) {
            return false;
        }

        AbstractBot bot = Environment.getBot();
        if (bot == null) {
            return false;
        }
        ForkJoinTask<?> existing = CONCURRENT_TASKS.getIfPresent(bot);
        if (existing != null && !existing.isDone()) {
            log.debug("Existing mouse move for this bot isn't done");
            return false;
        }
        CONCURRENT_TASKS.put(bot, bot.getPlatform().submit(() -> MouseCamera.turnTo(end)));
        return true;
    }

    public boolean isEnabled() {
        return OptionsTab.AllSettings.MIDDLE_MOUSE_CAMERA.isEnabled();
    }

    public boolean turnTo(Locatable end) {
        return turnTo(end, Variety.CAM_PREF_PITCH.asDouble());
    }

    public boolean turnTo(Locatable end, boolean centerMouse) {
        return turnTo(end, Variety.CAM_PREF_PITCH.asDouble(), centerMouse);
    }

    public boolean turnTo(Locatable end, double pitch) {
        final Player local = Players.getLocal();
        return local != null && turnTo(local, end, pitch, Variety.CAM_CENTER.asBoolean());
    }

    public boolean turnTo(Locatable start, Locatable end, boolean centerMouse) {
        if (start == null || end == null) {
            return false;
        }

        return turnTo(
            CommonMath.getAngleOf(start, centralPosition(end)),
            Math.max(0.1, -0.0179 * Distance.between(start, end) + 0.688),
            centerMouse
        );
    }

    public boolean turnTo(Locatable start, Locatable end, double pitch, boolean centerMouse) {
        if (start == null || end == null) {
            return false;
        }

        return turnTo(CommonMath.getAngleOf(start, centralPosition(end)), pitch, centerMouse);
    }

    public boolean turnTo(Locatable end, double pitch, boolean centerMouse) {
        if (end == null) {
            return false;
        }

        return turnTo(CommonMath.getAngleOf(centralPosition(end)), pitch, centerMouse);
    }

    public boolean turnTo(int yaw, double pitch) {
        return turnTo(yaw, pitch, Variety.CAM_CENTER.asBoolean());
    }

    public boolean turnTo(int yaw, double pitch, boolean centerMouse) {
        if (!isEnabled()) {
            return Camera.turnTo(yaw, pitch);
        }

        if (!Screen.isFixedMode()) {
            centerMouse = true;
        }

        log.info("Turning camera to yaw {} and pitch {} with centered mouse {}", yaw, pitch, centerMouse);
        if (yaw < 0 || yaw > 359) {
            log.warn("Attempted to turn to invalid yaw {}", yaw);
            return false;
        }

        if (pitch < 0 || pitch > 1) {
            log.warn("Attempted to turn to invalid pitch {}", pitch);
            return false;
        }

        int currentYaw = Camera.getYaw();
        double currentPitch = Camera.getPitch();
        int yawOffset = RotatableCommons.getYawOffset(currentYaw, yaw);
        double pitchOffset = currentPitch - pitch;

        if (Math.abs(yawOffset) < Variety.CAM_YAW_TOLERANCE.asDouble() && Math.abs(pitchOffset) < Variety.CAM_PITCH_TOLERANCE.asDouble()) {
            log.debug("yawOffset {} and pitchOffset {} are within tolerance", yawOffset, pitchOffset);
            return true;
        }

        int dx = (int) (yawOffset / YAW_CONVERSION);
        double dy = pitchOffset / PITCH_CONVERSION;
        log.trace("Yaw offset is {}, dx is {}", yawOffset, dx);
        log.trace("Pitch offset is {}, dy is {}", pitchOffset, dy);

        boolean right = dx < 0;
        boolean down = currentPitch > pitch;

        Shape screen = Projection.getViewport();
        if (screen == null) {
            screen = Screen.getBounds();
        }
        if (screen == null) {
            log.warn("Failed to resolve screen bounds");
            return false;
        }

        Rectangle2D rect = screen.getBounds2D();
        BoundingBox box = new BoundingBox(0, 0, Math.abs(dx), (int) Math.abs(dy));

        if (centerMouse) {
            box.move((int) ((rect.getWidth() / 2) - ((double) Math.abs(dx) / 2)), (int) ((rect.getHeight() / 2) - (Math.abs(dy) / 2)));
        } else {
            final Point mouse = Mouse.getPosition();
            if (mouse == null) {
                log.warn("Failed to resolve current mouse position");
                return false;
            }

            box.move(mouse.x, mouse.y);

            if (!right) {
                box.move(dx, 0);
            }
            if (down) {
                box.move(0, (int) dy);
            }
        }

        box.clamp((int) rect.getWidth(), (int) rect.getHeight(), 10);

        Pair<InteractablePoint, InteractablePoint> points = box.points(right, down);
        log.debug("Dragging mouse from {} to {}", points.getLeft(), points.getRight());

        double initialSpeed = Mouse.getSpeedMultiplier();
        Mouse.setSpeedMultiplier(initialSpeed * Variety.CAM_SENSITIVITY.asDouble());
        boolean result = drag(points.getLeft(), points.getRight());
        Mouse.setSpeedMultiplier(initialSpeed);
        Execution.delay((int) Variety.CAM_RELEASE_DELAY.asDouble(45));
        return result;
    }

    @Nullable
    private Coordinate centralPosition(Locatable locatable) {
        if (locatable == null) {
            return null;
        }

        return Optional.ofNullable(locatable.getArea()).map(Area.Rectangular::getCenter).orElseGet(locatable::getPosition);
    }

    private boolean drag(Interactable start, Interactable target) {
        if (Mouse.isPressed(Mouse.Button.WHEEL)) {
            log.warn("Middle mouse was already pressed when drag started!");
            Mouse.release(Mouse.Button.WHEEL);
            return false;
        }

        log.debug("Moving mouse to {}", start);
        if (!Mouse.move(start)) {
            log.debug("Failed to move mouse to {}", start);
            return false;
        }
        Execution.delay((int) Variety.CAM_RELEASE_DELAY.asDouble(45));


        if (!Mouse.press(Mouse.Button.WHEEL)) {
            log.warn("Failed to press middle mouse button");
            return false;
        }
        Execution.delay((int) Variety.CAM_RELEASE_DELAY.asDouble(45));

        log.debug("Moving mouse to {}", target);
        if (!Mouse.move(target)) {
            log.debug("Failed to move mouse to {}", target);
            Mouse.release(Mouse.Button.WHEEL);
            return false;
        }

        Execution.delay((int) Variety.CAM_RELEASE_DELAY.asDouble(45));
        return Mouse.release(Mouse.Button.WHEEL);
    }

    @Data
    @AllArgsConstructor
    private static class BoundingBox {

        int minX, minY, maxX, maxY;

        public void move(int x, int y) {
            minX += x;
            maxX += x;
            minY += y;
            maxY += y;
        }

        public void clamp(int width, int height, int padding) {
            if (minX < padding) {
                move(Math.abs(minX - padding), 0);
            } else if (maxX > width - padding) {
                move(-Math.abs(maxX - width) + padding, 0);
            }

            if (minY < padding) {
                move(0, Math.abs(minY - padding));
            } else if (maxY > height - padding) {
                move(0, -Math.abs(maxY - (height - padding)));
            }
        }

        public Pair<InteractablePoint, InteractablePoint> points(boolean right, boolean down) {
            InteractablePoint start = new InteractablePoint(right ? minX : maxX, down ? maxY : minY);
            InteractablePoint end = new InteractablePoint(right ? maxX : minX, down ? minY : maxY);
            return new Pair<>(start, end);
        }
    }

    @RequiredArgsConstructor
    private enum Variety {
        CAM_RELEASE_DELAY("mousecam_release_delay", () -> Random.nextGaussian(45, 90)),
        CAM_SENSITIVITY("mousecam_sensitivity", () -> Random.nextGaussian(0.6, 1.4)),
        CAM_PREF_PITCH("mousecam_pitch", () -> Random.nextGaussian(0.85, 1)),
        CAM_YAW_TOLERANCE("mousecam_tolerance_yaw", () -> Random.nextGaussian(4, 14)),
        CAM_PITCH_TOLERANCE("mousecam_tolerance_pitch", () -> Random.nextGaussian(0.04, 0.08)),
        CAM_CENTER("mousecam_center", () -> Random.nextGaussian(0, 100) > 20),
        ;

        private final String key;
        private final Supplier<Object> generator;

        public double asDouble() {
            return PlayerSense.getAsDouble(key, () -> (double) generator.get());
        }

        public double asDouble(double jitter) {
            double value = asDouble();
            return Random.nextDouble(value - jitter, value + jitter);
        }

        public boolean asBoolean() {
            return PlayerSense.getAsBoolean(key, () -> (boolean) generator.get());
        }
    }
}
