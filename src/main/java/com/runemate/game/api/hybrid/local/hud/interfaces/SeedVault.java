package com.runemate.game.api.hybrid.local.hud.interfaces;

import com.runemate.game.api.hybrid.entities.*;
import com.runemate.game.api.hybrid.entities.definitions.*;
import com.runemate.game.api.hybrid.local.*;
import com.runemate.game.api.hybrid.local.hud.*;
import com.runemate.game.api.hybrid.location.*;
import com.runemate.game.api.hybrid.queries.*;
import com.runemate.game.api.hybrid.queries.results.*;
import com.runemate.game.api.hybrid.region.*;
import com.runemate.game.api.hybrid.util.*;
import com.runemate.game.api.osrs.local.*;
import com.runemate.game.api.script.*;
import java.util.*;
import java.util.regex.*;
import java.util.stream.*;
import lombok.experimental.*;
import lombok.extern.log4j.*;
import org.jetbrains.annotations.*;

@Log4j2
@UtilityClass
public class SeedVault {

    private final int CONTAINER = 631;
    private final Pattern TITLE = Pattern.compile("Seed Vault");

    public static final Coordinate POSITION = new Coordinate(1246, 3741, 0);

    private final String WITHDRAW_ALL = "Withdraw-All";
    private final String WITHDRAW_ALL_BUT_ONE = "Withdraw-All-but-1";
    private final String WITHDRAW_X = "Withdraw-X";
    private final Pattern WITHDRAW_NUMBER_PATTERN = Regex.getPattern("^Withdraw-(\\d+)$");
    private final Pattern DEPOSIT_ALL_PATTERN = Regex.getPattern("^Deposit[- ]All$");
    private final Pattern DEPOSIT_X_PATTERN = Regex.getPattern("^Deposit[- ]X$");
    private final Pattern DEPOSIT_NUMBER_PATTERN = Regex.getPattern("^Deposit[- ](\\d+)$");

    public SpriteItemQueryBuilder newQuery() {
        return new SpriteItemQueryBuilder(Inventories.Documented.SEED_VAULT);
    }

    public boolean isAccessible() {
        return Skill.FARMING.getCurrentLevel() >= 45
            && AccountInfo.getType() != AccountInfo.AccountType.ULTIMATE_IRONMAN;
    }

    public boolean open() {
        if (isOpen()) {
            return true;
        }

        final var local = Players.getLocal();
        if (local == null) {
            return false;
        }

        final var object = instance();
        return object != null
            && object.isVisible()
            && object.interact("Open")
            && Execution.delayUntil(SeedVault::isOpen, local::isMoving, 1200, 1800);
    }

    public boolean close() {
        if (!isOpen()) {
            return true;
        }

        final var closeButton = Interfaces.newQuery()
            .containers(CONTAINER)
            .actions("Close")
            .results()
            .first();

        return closeButton != null
            && closeButton.click()
            && Execution.delayWhile(SeedVault::isOpen, 1200);
    }

    public @Nullable GameObject instance() {
        return GameObjects.newQuery().types(GameObject.Type.PRIMARY).names("Seed vault").results().first();
    }

    public boolean isOpen() {
        return !Interfaces.newQuery()
            .containers(CONTAINER)
            .types(InterfaceComponent.Type.LABEL)
            .texts(TITLE)
            .visible()
            .results()
            .isEmpty();
    }

    @Nullable
    public static InteractableRectangle getViewport() {
        final var component = Interfaces.getAt(CONTAINER, 11);
        return component != null ? component.getBounds() : null;
    }

    @Nullable
    public InteractableRectangle getBoundsOf(int index) {
        final var component = Interfaces.getAt(CONTAINER, 15, index);
        return component != null ? component.getBounds() : null;
    }

    @Nullable
    public SpriteItem getItemIn(final int slot) {
        return newQuery().indices(slot).results().first();
    }

    public int getQuantity(final int... ids) {
        return Items.getQuantity(getItems(), ids);
    }

    public int getQuantity(final String... names) {
        return Items.getQuantity(getItems(), names);
    }

    public int getQuantity(final Pattern... names) {
        return Items.getQuantity(getItems(), names);
    }

    public boolean contains(final int ids) {
        return Items.contains(getItems(), ids);
    }

    public boolean contains(final String names) {
        return Items.contains(getItems(), names);
    }

    public boolean contains(final Pattern names) {
        return Items.contains(getItems(), names);
    }

    public SpriteItemQueryResults getItems() {
        return newQuery().results();
    }

    public boolean withdraw(int id, int amount) {
        return withdraw(newQuery().ids(id).results().first(), amount);
    }

    public boolean withdraw(String name, int amount) {
        return withdraw(newQuery().names(name).results().first(), amount);
    }

    public boolean withdraw(Pattern name, int amount) {
        return withdraw(newQuery().names(name).results().first(), amount);
    }

    public boolean deposit(int id, int amount) {
        return deposit(Inventory.newQuery().ids(id).results().first(), amount);
    }

    public boolean deposit(String name, int amount) {
        return deposit(Inventory.newQuery().names(name).results().first(), amount);
    }

    public boolean deposit(Pattern name, int amount) {
        return deposit(Inventory.newQuery().names(name).results().first(), amount);
    }

    @SuppressWarnings("Duplicates")
    public boolean withdraw(final SpriteItem item, final int amount) {
        if (item == null) {
            return false;
        }
        log.info("Withdrawing {} {}", amount, item);
        if (item.hover()) {
            int availableQuantity = getQuantity(item.getId());
            final int quantityToWithdraw;
            if (amount == 0) {
                quantityToWithdraw = availableQuantity;
            } else if (amount == -1) {
                quantityToWithdraw = availableQuantity - 1;
            } else {
                quantityToWithdraw = amount;
            }

            if (quantityToWithdraw <= 0) {
                log.info("There must be at least 1 item available for withdrawal");
                return false;
            }

            ItemDefinition def = item.getDefinition();
            int emptySlots = Inventory.getEmptySlots();
            boolean willFillInventory = def != null && !def.stacks() && quantityToWithdraw >= emptySlots;
            boolean willEmptyBank = quantityToWithdraw >= availableQuantity;

            Set<String> actions = Menu.getItems().stream()
                .map(MenuItem::getAction)
                .collect(Collectors.toSet());

            Set<String> validActions = actions.stream()
                .filter(action -> {
                    int quantity;
                    Matcher matcher;
                    if (WITHDRAW_ALL.equals(action)) {
                        quantity = availableQuantity;
                    } else if (WITHDRAW_ALL_BUT_ONE.equals(action)) {
                        quantity = availableQuantity - 1;
                    } else if ((matcher = WITHDRAW_NUMBER_PATTERN.matcher(action)).find()) {
                        quantity = Integer.parseInt(matcher.group(1));
                    } else {
                        return false;
                    }
                    return quantity == quantityToWithdraw
                        || (willFillInventory && quantity >= emptySlots)
                        || (willEmptyBank && quantity >= availableQuantity);
                })
                .collect(Collectors.toSet());

            Pattern action;
            boolean manual = false;
            if (!validActions.isEmpty()) {
                //We make a pattern with all valid actions and let .interact() select the correct one (i.e left-click)
                action = Regex.getPatternForExactStrings(validActions.toArray(new String[0]));
            } else {
                //We default to Withdraw-X if no other action is valid
                if (actions.contains(WITHDRAW_X)) {
                    action = Regex.getPatternForExactString(WITHDRAW_X);
                    manual = true;
                } else {
                    log.warn("Action not found in {}", actions);
                    return false;
                }
            }

            log.debug("Using action {}", action);
            if (item.interact(action)) {
                if (manual && Execution.delayUntil(InputDialog::isOpen, 1200, 2400)) {
                    return InputDialog.enterAmount(quantityToWithdraw) &&
                        Execution.delayWhile(item::isValid, 1200, 2400);
                }
                return Execution.delayWhile(item::isValid, 1200, 2400);
            }
            return Execution.delayWhile(item::isValid, 1200, 2400);
        }
        return false;
    }

    public static boolean deposit(final SpriteItem item, final int amount) {
        if (item == null || !item.hover()) {
            return false;
        }
        log.info("Depositing {} {}", amount, item);
        int invQuantity = Inventory.getQuantity(item.getId());
        final int quantityToDeposit = amount == 0 ? invQuantity : amount;
        boolean willDepositRemaining = quantityToDeposit >= invQuantity;

        var actions = Menu.getItems().stream()
            .map(MenuItem::getAction)
            .collect(Collectors.toSet());

        var validActions = actions.stream()
            .filter(action -> {
                int quantity;
                Matcher matcher;
                if (DEPOSIT_ALL_PATTERN.matcher(action).find()) {
                    quantity = invQuantity;
                } else if ((matcher = DEPOSIT_NUMBER_PATTERN.matcher(action)).find()) {
                    quantity = Integer.parseInt(matcher.group(1));
                } else {
                    return false;
                }
                return quantity == quantityToDeposit
                    || (willDepositRemaining && quantity >= invQuantity);
            })
            .collect(Collectors.toSet());

        Pattern action;
        boolean manual = false;
        if (!validActions.isEmpty()) {
            //We make a pattern with all valid actions and let .interact() select the correct one (i.e left-click)
            action = Regex.getPatternForExactStrings(validActions.toArray(new String[0]));
        } else {
            //We default to Withdraw-X if no other action is valid
            if (actions.stream().anyMatch(a -> DEPOSIT_X_PATTERN.matcher(a).find())) {
                action = DEPOSIT_X_PATTERN;
                manual = true;
            } else {
                log.warn("Action not found in {}", actions);
                return false;
            }
        }

        log.debug("Using action {}", action);
        if (item.interact(action)) {
            if (manual && Execution.delayUntil(InputDialog::isOpen, 1400, 2200)) {
                return InputDialog.enterAmount(amount) && Execution.delayWhile(item::isValid, 1400, 2200);
            }
            return Execution.delayWhile(item::isValid, 1400, 2200);
        }
        return false;
    }
}