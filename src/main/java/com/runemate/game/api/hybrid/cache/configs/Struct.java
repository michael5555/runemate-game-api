package com.runemate.game.api.hybrid.cache.configs;

import com.runemate.game.cache.io.*;
import com.runemate.game.cache.item.*;
import java.io.*;
import java.util.*;
import lombok.*;

@RequiredArgsConstructor
@Getter
@ToString
public class Struct extends IncrementallyDecodedItem {

    private final int id;
    private final Map<Integer, Object> params = new HashMap<>();

    @Override
    protected void decode(final Js5InputStream stream, final int opcode) throws IOException {
        if (opcode == 249) {
            var length = stream.readUnsignedByte();
            for (int i = 0; i < length; i++) {
                boolean isString = stream.readUnsignedByte() == 1;
                int key = stream.read24BitInt();
                Object value;

                if (isString) {
                    value = stream.readCStyleLatin1String();
                } else {
                    value = stream.readInt();
                }

                params.put(key, value);
            }
        }
    }
}
