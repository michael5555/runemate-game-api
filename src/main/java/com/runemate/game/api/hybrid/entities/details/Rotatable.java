package com.runemate.game.api.hybrid.entities.details;

import com.runemate.game.api.hybrid.util.*;

/**
 * An entity that can be rotated
 */
public interface Rotatable {
    /**
     * For internal usage only. Subject to removal without notice.
     */
    int getHighPrecisionOrientation();

    /**
     * Gets the current orientation as an angle.
     */
    int getOrientationAsAngle();

    /**
     * Checks if this rotatable entity is facing a locatable object
     */
    default boolean isFacing(Locatable locatable) {
        return this instanceof Locatable && RotatableCommons.isFacing((Locatable) this, locatable);
    }
}
