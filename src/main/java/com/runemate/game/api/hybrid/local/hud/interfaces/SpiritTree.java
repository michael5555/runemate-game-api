package com.runemate.game.api.hybrid.local.hud.interfaces;

import com.runemate.game.api.hybrid.entities.*;
import com.runemate.game.api.hybrid.local.*;
import com.runemate.game.api.hybrid.location.*;
import com.runemate.game.api.hybrid.region.*;
import com.runemate.game.api.script.*;
import java.util.regex.*;
import lombok.*;
import lombok.extern.log4j.*;

@Log4j2
@RequiredArgsConstructor
public enum SpiritTree {
    TREE_GNOME_VILLAGE(new Coordinate(2542, 3170, 0), "Tree Gnome Village", '1'),
    GNOME_STRONGHOLD(new Coordinate(2461, 3444, 0), "Gnome Stronghold", '2'),
    BATTLEFIELD_OF_KHAZARD(new Coordinate(2555, 3259, 0), "Battlefield of Khazard", '3'),
    GRAND_EXCHANGE(new Coordinate(3185, 3508, 0), "Grand Exchange", '4'),
    FELDIP_HILLS(new Coordinate(2488, 2850, 0), "Feldip Hills", '5'),
    PRIFDDINAS(new Coordinate(3274, 6123, 0), "Prifddinas", '6');

    @Getter
    private final Coordinate position;
    @Getter
    private final String name;
    @Getter
    private final char hotkey;

    private static final Pattern NAME = Pattern.compile("(Christmas-)?Spirit(ual Fairy)? Tree", Pattern.CASE_INSENSITIVE);
    private static final Pattern OPEN_ACTION = Pattern.compile("Travel|Tree");

    /**
     * @return true if the teleport was successful.
     */
    public boolean teleport() {
        if(getPosition().isLoaded()){
            return true;
        }

        Player local = Players.getLocal();
        if (local == null) {
            return false;
        }

        InterfaceComponent component = getInterfaceComponent();
        if (component != null) {
            return component.interact("Continue")
                && Execution.delayUntil(getPosition()::isLoaded, () -> !local.isIdle(), 2400);
        }

        GameObject object = getNearestObject();
        if (object == null) {
            return false;
        }

        if (object.getVisibility() < 100) {
            Camera.turnTo(object);
        }

        return object.interact(OPEN_ACTION)
            && Execution.delayUntil(SpiritTree::isInterfaceOpen, local::isMoving, 1200);
    }

    /**
     * @return the teleport label of the Spirit Tree location
     */
    private InterfaceComponent getInterfaceComponent() {
        return Interfaces.newQuery()
            .containers(187)
            .types(InterfaceComponent.Type.LABEL)
            .textContains(getName())
            .visible()
            .results()
            .first();
    }

    /**
     * @return the nearest Spirit tree GameObject, if one is available.
     */
    public static GameObject getNearestObject() {
        return GameObjects.newQuery()
            .types(GameObject.Type.PRIMARY)
            .names(NAME)
            .results()
            .nearest();
    }

    /**
     * @return true if the teleport interface is open.
     */
    public static boolean isInterfaceOpen() {
        return !Interfaces.newQuery()
            .types(InterfaceComponent.Type.LABEL)
            .containers(187)
            .textContains("Spirit Tree")
            .grandchildren(true)
            .results()
            .isEmpty();
    }

}