package com.runemate.game.api.hybrid.local;

import com.runemate.game.api.hybrid.entities.details.Locatable;
import com.runemate.game.api.hybrid.location.Area;
import com.runemate.game.api.hybrid.location.Coordinate;
import com.runemate.game.api.hybrid.region.Players;
import com.runemate.game.api.hybrid.local.Varbit;
import com.runemate.game.api.hybrid.local.Varbits;
import com.runemate.game.api.hybrid.local.VarbitID;
import java.util.regex.*;

public class Wilderness {

    //Bounty hunter
    private static final Area WILDERNESS_AREA_0 = new Area.Rectangular(new Coordinate(3328, 3968, 0), new Coordinate(3519, 4159, 3));

    // Bounty hunter bank
    private static final Area WILDERNESS_AREA_1 = new Area.Rectangular(new Coordinate(3413, 4053, 0), new Coordinate(3434, 4074, 3));

    // Wilderness main (reduce max-Y by 3 regions)
    private static final Area WILDERNESS_AREA_2 = new Area.Rectangular(new Coordinate(2944, 3520, 0), new Coordinate(3391, 4351, 3));

    // God Wars Dungeon (wilderness)
    private static final Area WILDERNESS_AREA_3 = new Area.Rectangular(new Coordinate(3008, 10112, 0), new Coordinate(3071, 10175, 3));

    // Ve'tion boss
    private static final Area WILDERNESS_AREA_4 = new Area.Rectangular(new Coordinate(3264, 10176, 0), new Coordinate(3327, 10239, 3));
    private static final Area WILDERNESS_AREA_8 = new Area.Rectangular(new Coordinate(1856, 11520, 0), new Coordinate(1919, 11583, 3));

    // Spider boss
    private static final Area WILDERNESS_AREA_5 = new Area.Rectangular(new Coordinate(3392, 10176, 0), new Coordinate(3455, 10239, 3));
    private static final Area WILDERNESS_AREA_9 = new Area.Rectangular(new Coordinate(1600, 11520, 0), new Coordinate(1663, 11583, 3));

    // Bear boss
    private static final Area WILDERNESS_AREA_7 = new Area.Rectangular(new Coordinate(1728, 11520, 0), new Coordinate(1791, 11583, 3));
    private static final Area WILDERNESS_AREA_6 = new Area.Rectangular(new Coordinate(3328, 10304, 0), new Coordinate(3391, 10367, 3));
    private static final Area WILDERNESS_AREA_10 = new Area.Rectangular(new Coordinate(3328, 10240, 0), new Coordinate(3391, 10303, 3));

    // Manually broke area 11 into 3 subsections to avoid misidentifying some non-wilderness zones
//    private static final Area WILDERNESS_AREA_11 = new Area.Rectangular(new Coordinate(2944, 9920, 0), new Coordinate(3455, 10879, 3));
    private static final Area WILDERNESS_AREA_11 = new Area.Rectangular(new Coordinate(3072, 9920, 0), new Coordinate(3135, 10047, 3));
    private static final Area WILDERNESS_AREA_12 = new Area.Rectangular(new Coordinate(2944, 9984, 0), new Coordinate(3071, 10367, 3));
    private static final Area WILDERNESS_AREA_13 = new Area.Rectangular(new Coordinate(3136, 10048, 0), new Coordinate(3455, 10367, 3));

    /**
     * Based on <a href="https://github.com/Joshua-F/cs2-scripts/blob/master/scripts/%5Bproc,wilderness_level%5D.cs2">script 384</a>
     * <pre>
     {@code
        def_int $int0 = 0;
        if (~inzone(0_52_62_0_0, 3_54_64_63_63, coord) = 1 & ~inzone(0_53_63_21_21, 3_53_63_42_42, coord) = 0) {
        $int0 = 5;
        } else if (~inzone(0_46_55_0_0, 3_52_67_63_63, coord) = 1) {
        $int0 = calc((coordz(coord) - 55 * 64) / 8 + 1);
        } else if (~inzone(0_47_158_0_0, 3_47_158_63_63, coord) = 1) {
        $int0 = calc((coordz(coord) - 155 * 64) / 8 - 1);
        } else if (~inzone(0_51_159_0_0, 3_51_159_63_63, coord) = 1) {
        $int0 = 35;
        } else if (~inzone(0_53_159_0_0, 3_53_159_63_63, coord) = 1) {
        $int0 = 35;
        } else if (~inzone(0_52_161_0_0, 3_52_161_63_63, coord) = 1) {
        $int0 = 40;
        } else if (~inzone(0_27_180_0_0, 3_27_180_63_63, coord) = 1) {
        $int0 = 21;
        } else if (~inzone(0_29_180_0_0, 3_29_180_63_63, coord) = 1) {
        $int0 = 21;
        } else if (~inzone(0_25_180_0_0, 3_25_180_63_63, coord) = 1) {
        $int0 = 29;
        } else if (~inzone(0_52_160_0_0, 3_52_160_63_63, coord) = 1) {
        $int0 = calc(33 + scale(calc(coordz(coord) % 64 - 6), 50, 7));
        } else if (~inzone(0_46_155_0_0, 3_53_169_63_63, coord) = 1) {
        $int0 = calc((coordz(coord) - 155 * 64) / 8 + 1);
        }
        return($int0);
     }
     * </pre>
     */
    public static short getDepth(Locatable locatable) {
        Coordinate position;
        if (locatable != null && (position = locatable.getPosition()) != null) {
            int level = 0;
            if (WILDERNESS_AREA_0.contains(position) & !WILDERNESS_AREA_1.contains(position)) {
                level = 5;
            } else if (WILDERNESS_AREA_2.contains(position)) {
                level = (position.getY() - 55 * 64) / 8 + 1;
            } else if (WILDERNESS_AREA_3.contains(position)) {
                level = (position.getY() - 155 * 64) / 8 - 1;
            } else if (WILDERNESS_AREA_4.contains(position)) {
                level = 35;
            } else if (WILDERNESS_AREA_5.contains(position)) {
                level = 35;
            } else if (WILDERNESS_AREA_6.contains(position)) {
                level = 40;
            } else if (WILDERNESS_AREA_7.contains(position)) {
                level = 21;
            } else if (WILDERNESS_AREA_8.contains(position)) {
                level = 21;
            } else if (WILDERNESS_AREA_9.contains(position)) {
                level = 29;
            } else if (WILDERNESS_AREA_10.contains(position)) {
                level = 33 + (position.getY() % 64 - 6) * 50 / 7;
            } else if (WILDERNESS_AREA_11.contains(position) || WILDERNESS_AREA_12.contains(position) || WILDERNESS_AREA_13.contains(position)) {
                level = (position.getY() - 155 * 64) / 8 + 1;
            }
            return (short) level;
        }
        return 0;
    }

    public static boolean isInWilderness() {
        Varbit varbit = Varbits.load(VarbitID.IN_WILDERNESS.getId());
        return varbit != null && varbit.getValue() == 1;
    }

    public static short getDepth() {
        return getDepth(Players.getLocal());
    }


}
