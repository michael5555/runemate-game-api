package com.runemate.game.api.hybrid.location.navigation;

import com.runemate.client.boot.*;
import com.runemate.client.game.open.*;
import com.runemate.game.api.client.*;
import com.runemate.game.api.hybrid.*;
import com.runemate.game.api.hybrid.entities.*;
import com.runemate.game.api.hybrid.local.*;
import com.runemate.game.api.hybrid.local.hud.interfaces.*;
import com.runemate.game.api.hybrid.location.*;
import com.runemate.game.api.hybrid.location.navigation.web.*;
import com.runemate.game.api.hybrid.queries.results.*;
import com.runemate.game.api.hybrid.region.*;
import com.runemate.game.api.osrs.location.navigation.*;
import com.runemate.game.api.script.*;
import com.runemate.game.api.script.annotations.*;
import java.util.*;
import java.util.regex.*;
import lombok.experimental.*;
import org.apache.commons.lang3.*;
import org.jetbrains.annotations.*;

@UtilityClass
public final class Traversal {

    private static final Pattern STAMINA_ENHANCERS = Pattern.compile("^Stamina (mix|potion)\\([1-4]\\)$");

    private static Web defaultWeb;

    /**
     * Gets the local players walking destination (the minimap flags coordinate)
     */

    @Nullable
    public static Coordinate getDestination() {
        final int destX = OpenClient.getDestinationX(), destY = OpenClient.getDestinationY();
        if (destX > 0 && destY > 0) {
            final Coordinate base = Scene.getBase();
            return base.derive(destX, destY);
        }
        return null;
    }


    public static boolean isRunEnabled() {
        return OSRSTraversal.isRunEnabled();
    }

    /**
     * @return Run energy as a percentage
     */
    public static int getRunEnergy() {
        return OSRSTraversal.getRunEnergy();
    }

    public static boolean toggleRun() {
        return OSRSTraversal.toggleRun();
    }

    /**
     * Determines if your player's stamina is currently enhanced due to the usage of a stamina potion (OSRS only)
     *
     * @return true if your stamina (run regeneration rate) is currently boosted because of a stamina potion.
     */
    public static boolean isStaminaEnhanced() {
        Varbit staminaPotionVarbit = Varbits.load(25);
        if (staminaPotionVarbit != null) {
            return staminaPotionVarbit.getValue() == 1;
        }
        return false;
    }

    /**
     * Drinks any stamina potions or mixes in your inventory (OSRS only)
     *
     * @return true is a stamina enhancer became active.
     */
    public static boolean drinkStaminaEnhancer(boolean delayUntilStaminaIsEnhanced) {
        var potion = getStaminaEnhancers().sortByDistanceFromMouse().first();
        if (potion != null && potion.interact("Drink")) {
            return !delayUntilStaminaIsEnhanced || Execution.delayUntil(() -> !potion.isValid() || isStaminaEnhanced(), 900, 1800);
        }
        return false;
    }

    /**
     * Drinks any stamina potions or mixes in your inventory (OSRS only)
     *
     * @return true is a stamina enhancer became active.
     */
    public static boolean drinkStaminaEnhancer() {
        return drinkStaminaEnhancer(true);
    }

    public static SpriteItemQueryResults getStaminaEnhancers() {
        return Inventory.newQuery().names(STAMINA_ENHANCERS).unnoted().actions("Drink").results();
    }

    static boolean hasStaminaEnhancer() {
        return !getStaminaEnhancers().isEmpty();
    }
}