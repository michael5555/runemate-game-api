package com.runemate.game.api.hybrid.local.hud;

import com.runemate.game.api.hybrid.entities.details.*;
import com.runemate.game.api.hybrid.input.*;
import com.runemate.game.api.hybrid.util.*;
import com.runemate.game.api.hybrid.util.calculations.*;
import java.awt.*;
import java.awt.geom.*;
import java.util.regex.*;
import javafx.scene.canvas.*;

public class InteractablePolygon implements Shape, Interactable, Renderable {
    private final Polygon polygon;

    public InteractablePolygon(final Polygon polygon) {
        if (polygon == null) {
            throw new IllegalArgumentException("The polygon argument cannot be null.");
        }
        this.polygon = polygon;
    }

    @Override
    public boolean isVisible() {
        return true;
    }

    @Override
    public double getVisibility() {
        return 100;
    }

    @Override
    public boolean hasDynamicBounds() {
        return false;
    }

    @Override
    public InteractablePoint getInteractionPoint(Point origin) {
        final Rectangle bounds = getBounds();
        if (bounds.width == 0 || bounds.height == 0) {
            return null;
        }
        InteractablePoint random = null;
        while (random == null || !polygon.contains(random)) {
            random = new InteractablePoint(
                Random.nextInt(bounds.x, bounds.x + bounds.width),
                Random.nextInt(bounds.y, bounds.y + bounds.height)
            );
        }
        return random;
    }

    @Override
    public boolean contains(Point point) {
        return polygon.contains(point);
    }

    @Override
    public boolean contains(Rectangle2D rectangle) {
        return polygon.contains(rectangle);
    }

    @Override
    public boolean click() {
        return Mouse.click(this, Mouse.Button.LEFT);
    }

    @Override
    public final boolean interact(final String action) {
        return interact(action, (Pattern) null);
    }

    @Override
    public boolean interact(Pattern action) {
        return interact(action, (Pattern) null);
    }

    @Override
    public final boolean interact(final Pattern action, final Pattern target) {
        return Menu.click(this, action, target);
    }

    @Override
    public boolean interact(Pattern action, String target) {
        Pattern targetPattern = null;
        if (target != null) {
            targetPattern = Regex.getPatternForExactString(target);
        }
        return interact(action, targetPattern);
    }

    @Override
    public boolean interact(String action, Pattern target) {
        Pattern actionPattern = null;
        if (action != null) {
            actionPattern = Regex.getPatternForExactString(action);
        }
        return interact(actionPattern, target);
    }

    @Override
    public String toString() {
        return "InteractablePolygon[" + polygon + ']';
    }

    @Override
    public Rectangle getBounds() {
        return polygon.getBounds();
    }

    @Override
    public Rectangle2D getBounds2D() {
        return polygon.getBounds2D();
    }

    @Override
    public boolean contains(double x, double y) {
        return polygon.contains(x, y);
    }

    @Override
    public boolean contains(Point2D p) {
        return polygon.contains(p);
    }

    @Override
    public boolean intersects(double x, double y, double w, double h) {
        return polygon.intersects(x, y, w, h);
    }

    @Override
    public boolean intersects(Rectangle2D r) {
        return polygon.intersects(r);
    }

    @Override
    public boolean contains(double x, double y, double w, double h) {
        return polygon.contains(x, y, w, h);
    }

    @Override
    public PathIterator getPathIterator(AffineTransform at) {
        return polygon.getPathIterator(at);
    }

    @Override
    public PathIterator getPathIterator(AffineTransform at, double flatness) {
        return polygon.getPathIterator(at, flatness);
    }

    @Override
    public void render(Graphics2D g2d) {
        render(g2d, false);
    }

    public void render(Graphics2D g2d, boolean fill) {
        if (fill) {
            g2d.fillPolygon(polygon.xpoints, polygon.ypoints, polygon.npoints);
        } else {
            g2d.drawPolygon(polygon);
        }
    }

    @Override
    public void render(GraphicsContext gc) {
        render(gc, false);
    }

    public void render(GraphicsContext gc, boolean fill) {
        if (fill) {
            gc.fillPolygon(Renderable.convert(polygon.xpoints), Renderable.convert(polygon.ypoints),
                polygon.npoints
            );
        } else {
            gc.strokePolygon(Renderable.convert(polygon.xpoints),
                Renderable.convert(polygon.ypoints), polygon.npoints
            );
        }
    }
}
