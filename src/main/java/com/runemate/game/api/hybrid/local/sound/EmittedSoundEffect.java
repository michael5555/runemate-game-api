package com.runemate.game.api.hybrid.local.sound;

import com.runemate.game.api.client.*;
import com.runemate.game.api.hybrid.*;
import com.runemate.game.api.hybrid.entities.*;
import com.runemate.game.api.hybrid.entities.definitions.*;
import com.runemate.game.api.script.annotations.*;
import org.apache.commons.lang3.*;

public interface EmittedSoundEffect {
    int getAmbientSoundId();

    int[] getShufflingSoundIds();

    int getAudibleRadius();

    GameObjectDefinition getEmittingObject();

    int getMinimumCyclesBeforeShuffle();

    int getMaximumCyclesBeforeShuffle();
}
