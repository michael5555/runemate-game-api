package com.runemate.game.api.hybrid.location.navigation.web;

import com.runemate.game.api.hybrid.entities.details.*;
import com.runemate.game.api.hybrid.location.navigation.*;
import com.runemate.game.api.hybrid.location.navigation.web.requirements.*;
import com.runemate.game.api.hybrid.region.*;
import com.runemate.game.api.osrs.location.navigation.web.vertex_types.teleports.*;
import java.io.*;
import java.util.*;
import org.jetbrains.annotations.*;

/**
 * A chaining builder object used for building WebPaths.
 */
public class WebPathBuilder {
    private final Web web;
    private boolean teleports = true, minigameTeleports = true, lodestoneTeleports = true;
    private boolean retrieveItems = true;
    private boolean debugMode;
    private boolean avoidWilderness = false;

    protected WebPathBuilder(Web web) {
        this.web = web;
    }

    public static WebPathBuilder create(Web web) {
        if (web == null) {
            throw new IllegalArgumentException("The provided Web cannot be null.");
        }
        return new WebPathBuilder(web);
    }

    /**
     * Sets whether or not to consider using teleports during path generation
     */
    public WebPathBuilder useTeleports(boolean use) {
        this.teleports = use;
        return this;
    }

    /**
     * Sets whether or not to consider using minigame teleports during path generation
     */
    public WebPathBuilder useMinigameTeleports(boolean use) {
        this.minigameTeleports = use;
        return this;
    }

    /**
     * Sets whether or not to consider using lodestones during path generation
     */
    public WebPathBuilder useLodestoneTeleports(boolean use) {
        this.lodestoneTeleports = use;
        return this;
    }

    /**
     * Not properly implemented, should not have been exposed yet.
     *
     * @param retrieve
     * @return
     */
    @Deprecated
    public WebPathBuilder retrieveItems(boolean retrieve) {
        this.retrieveItems = retrieve;
        return this;
    }

    /**
     * Not properly implemented, should not have been exposed yet.
     *
     * @param avoid
     * @return
     */
    @Deprecated
    public WebPathBuilder avoidWilderness(boolean avoid) {
        this.avoidWilderness = avoid;
        return this;
    }

    @Deprecated
    public final WebPathBuilder useDebugMode(boolean value) {
        this.debugMode = value;
        return this;
    }

    @Nullable
    public WebPath buildTo(Locatable destination) {
        return this.build(Players.getLocal(), destination);
    }

    @Nullable
    public WebPath build(Locatable start, Locatable destination) {
        return this.build(start, destination, null, teleports, minigameTeleports,
            lodestoneTeleports, retrieveItems, avoidWilderness
        );
    }

    @Nullable
    public WebPath buildTo(Landmark destination) {
        return this.build(Players.getLocal(), destination);
    }

    @Nullable
    public WebPath build(Locatable start, Landmark destination) {
        return this.build(start, null, destination, teleports, minigameTeleports,
            lodestoneTeleports, retrieveItems, avoidWilderness
        );
    }

    @Nullable
    protected WebPath build(
        Locatable start_, Locatable locatable_dest, Landmark landmark_dest,
        boolean useTeleports, boolean minigameTeleports,
        boolean lodestoneTeleports, boolean retrieveItems,
        boolean avoidWilderness
    ) {
        if (start_ == null || locatable_dest == null && landmark_dest == null) {
            return null;
        }
        WebVertex start = web.getVertexNearestTo(start_);
        WebVertex destination;
        if (locatable_dest != null) {
            destination = web.getVertexNearestTo(locatable_dest);
        } else {
            destination = web.getVertexNearestTo(start_, landmark_dest.getVertexType());
        }
        if (start == null || destination == null) {
            return null;
        }
        HashSet<WebVertex> destinations = new HashSet<>();
        destinations.add(start);
        if (useTeleports) {
            destinations.addAll(web.getTeleports());
            if (!minigameTeleports) {
                destinations.removeIf(wv -> wv instanceof MinigameTeleportVertex);
            }
        }
        PriorityQueue<WebPath> paths = new PriorityQueue<>(new WebPathBuilder.PathComparator());
        HashMap<WebVertex, Double> distances = new HashMap<>();
        HashMap<WebVertex, WebVertex> previous = new HashMap<>();
        PriorityQueue<WebVertex> unvisited =
            new PriorityQueue<>(new WebPathBuilder.VertexComparator(distances));
        distances.put(destination, 0.0D);
        unvisited.add(destination);
        while (!unvisited.isEmpty()) {
            WebVertex current = unvisited.poll();
            if (destinations.contains(current)) {
                double pathCost = distances.get(current);
                ArrayList<WebVertex> vertices = new ArrayList<>();
                while (current != null) {
                    vertices.add(current);
                    current = previous.get(current);
                }
                WebPath path = new WebPath(vertices, pathCost);
                paths.add(path);
                if (debugMode || path.areRequirementsMet()) {
                    break;
                }
                continue;
            }
            Double cdistance = distances.getOrDefault(current, (double) Integer.MAX_VALUE);
            for (WebVertex neighbor : current.getInputs()) {
                Double ecost = current.getInputCost(neighbor);
                if (ecost == null) {
                    throw new IllegalStateException(
                        "ecost is null for " + current + " -> " + neighbor);
                }
                double nndistance = cdistance + ecost;
                Double pndistance = distances.get(neighbor);
                if (pndistance == null || nndistance < pndistance) {
                    distances.put(neighbor, nndistance);
                    previous.put(neighbor, current);
                    unvisited.add(neighbor);
                }
            }
        }
        if (paths.isEmpty()) {
            return null;
        }
        return this.getOptimizedPathQueue(paths).poll();
    }

    /*protected List<WebPath> buildAllWithRequirementsMet() {
        // TODO: 6/7/2016
    }*/

    private PriorityQueue<WebPath> getOptimizedPathQueue(PriorityQueue<WebPath> paths) {
        PriorityQueue<WebPath> cleaned;
        if (paths.isEmpty()) {
            return paths;
        }
        HashMap<Set<WebRequirement>, WebPath> map = new HashMap<>();
        for (WebPath current : paths) {
            Set<WebRequirement> requirements = current.getRequirements();
            if (map.containsKey(requirements)) {
                WebPath best = map.get(requirements);
                if (current.getTraversalCost() < best.getTraversalCost()) {
                    map.put(requirements, current);
                }
            } else {
                map.put(requirements, current);
            }
        }
        cleaned = new PriorityQueue<>(paths.comparator());
        cleaned.addAll(map.values());
        if (cleaned.size() > 1) {
            WebPath lowest_cost = cleaned.peek();
            if (lowest_cost.getRequirements().isEmpty()) {
                cleaned = new PriorityQueue<>(cleaned.comparator());
                cleaned.add(lowest_cost);
                return cleaned;
            }
        }
        return cleaned;
    }

    private static final class VertexComparator implements Comparator<WebVertex>, Serializable {
        private final HashMap<WebVertex, Double> distances;

        private VertexComparator(HashMap<WebVertex, Double> distances) {
            this.distances = distances;
        }

        @Override
        public int compare(WebVertex o1, WebVertex o2) {
            Double d1 = distances.get(o1);
            if (d1 == null) {
                return distances.containsKey(o2) ? -1 : 0;
            }
            Double d2 = distances.get(o2);
            if (d2 == null) {
                return 1;
            }
            return Double.compare(d1, d2);
        }
    }

    private static class PathComparator implements Comparator<WebPath>, Serializable {
        @Override
        public int compare(WebPath o1, WebPath o2) {
            return Double.compare(o1.getTraversalCost(), o2.getTraversalCost());
        }
    }
}
