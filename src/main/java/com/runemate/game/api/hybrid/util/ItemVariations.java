package com.runemate.game.api.hybrid.util;

import com.fasterxml.jackson.core.type.*;
import com.fasterxml.jackson.databind.*;
import com.google.common.collect.*;
import java.io.*;
import java.util.*;

public class ItemVariations {

    private static final Map<Integer, Integer> MAPPINGS;
    private static final Multimap<Integer, Integer> INVERTED_MAPPINGS;

    static {

        ObjectMapper mapper = new ObjectMapper();
        Map<String, List<Integer>> variations;

        try (InputStream in = ItemVariations.class.getResourceAsStream("/item_variations.json")) {
            variations = mapper.readValue(in, new TypeReference<>() {});
        } catch (IOException e) {
            throw new RuntimeException("Failed to load item variations", e);
        }

        ImmutableMap.Builder<Integer, Integer> builder = new ImmutableMap.Builder<>();
        ImmutableMultimap.Builder<Integer, Integer> invertedBuilder = new ImmutableMultimap.Builder<>();
        for (List<Integer> value : variations.values()) {
            final Iterator<Integer> iterator = value.iterator();
            final int base = iterator.next();

            while (iterator.hasNext()) {
                final int id = iterator.next();
                builder.put(id, base);
                invertedBuilder.put(base, id);
            }

            builder.put(base, base);
        }

        MAPPINGS = builder.build();
        INVERTED_MAPPINGS = invertedBuilder.build();
    }


    public static int getBase(int itemId) {
        return MAPPINGS.getOrDefault(itemId, itemId);
    }

    public static Collection<Integer> getVariations(int itemId) {
        return INVERTED_MAPPINGS.asMap().getOrDefault(itemId, Collections.singletonList(itemId));
    }

}
