package com.runemate.game.api.hybrid.local;

/**
 * @see Varbit
 */
@Deprecated
public class Varpbit {
    private final int varp_type;
    private final Varp varp;
    private final int id, most_significant_bit, least_significant_bit;

    public Varpbit(
        int id, int varp_type, int varp_index, int most_significant_bit,
        int least_significant_bit
    ) {
        this.id = id;
        this.varp_type = varp_type;
        this.varp = Varps.getAt(varp_index);
        this.most_significant_bit = most_significant_bit;
        this.least_significant_bit = least_significant_bit;
    }

    @Deprecated
    public int getIndex() {
        return getId();
    }

    public int getId() {
        return id;
    }

    public int getValue() {
        return varp.getValueOfBitRange(least_significant_bit, most_significant_bit);
    }

    public int getType() {
        return varp_type;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + most_significant_bit;
        result = 31 * result + least_significant_bit;
        return result;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Varpbit)) {
            return false;
        }
        Varpbit varpbit = (Varpbit) o;
        return id == varpbit.id
            && most_significant_bit == varpbit.most_significant_bit
            && least_significant_bit == varpbit.least_significant_bit;

    }

    @Override
    public String toString() {
        return "Varpbit " + id;
    }
}
