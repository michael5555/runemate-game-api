package com.runemate.game.api.hybrid.local;

import com.runemate.client.game.open.*;
import java.util.*;

/**
 * Used for retrieving Varp objects (local player variables).
 * Varps are known to contain data related to questing, farming, and other local player data.
 * Also known as "Settings" in other APIs
 */
public final class Varps {
    private Varps() {
    }

    /**
     * Gets a list of all varps
     */
    public static List<Varp> getLoaded() {
        int count = OpenVarps.getCount();
        if (count == 0) {
            return Collections.emptyList();
        }
        final List<Varp> varps = new ArrayList<>(count);
        for (int index = 0; index < count; ++index) {
            varps.add(new Varp(index));
        }
        return varps;
    }

    /**
     * Gets the varp at the specified index
     */
    public static Varp getAt(final int index) {
        return new Varp(index);
    }

    /**
     * Gets a snapshot of all the values of the loaded varps.
     */
    public static int[] snapshot() {
        return OpenVarps.getAll();
    }
}
