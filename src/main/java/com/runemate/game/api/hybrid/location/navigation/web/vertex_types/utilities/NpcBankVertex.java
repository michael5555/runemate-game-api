package com.runemate.game.api.hybrid.location.navigation.web.vertex_types.utilities;

import com.runemate.game.api.hybrid.entities.*;
import com.runemate.game.api.hybrid.location.*;
import com.runemate.game.api.hybrid.location.navigation.web.requirements.*;
import com.runemate.game.api.hybrid.location.navigation.web.vertex_types.*;
import com.runemate.game.api.hybrid.queries.*;
import com.runemate.game.api.hybrid.region.*;
import com.runemate.game.api.hybrid.util.*;
import java.io.*;
import java.util.*;
import java.util.regex.*;
import lombok.*;
import org.apache.commons.lang3.builder.*;

public class NpcBankVertex extends BankVertex implements SerializableVertex {
    private double movementRadius;

    public NpcBankVertex(
        String name, String action, Coordinate position,
        Collection<WebRequirement> requirements
    ) {
        this(Regex.getPatternForExactString(name), Regex.getPatternForExactString(action), 1,
            position, requirements
        );
    }

    public NpcBankVertex(
        String name, String action, double movementRadius, Coordinate position,
        Collection<WebRequirement> requirements
    ) {
        this(Regex.getPatternForExactString(name), Regex.getPatternForExactString(action),
            movementRadius, position, requirements
        );
    }

    public NpcBankVertex(
        Pattern name, Pattern action, Coordinate position,
        Collection<WebRequirement> requirements
    ) {
        this(name, action, 1, position, requirements);
    }

    public NpcBankVertex(
        Pattern name, Pattern action, double movementRadius, Coordinate position,
        Collection<WebRequirement> requirements
    ) {
        super(name, action, position, requirements);
        this.movementRadius = movementRadius;
    }

    public NpcBankVertex(
        Coordinate position, Collection<WebRequirement> requirements,
        Collection<WebRequirement> forbiddingRequirements, int protocol,
        ObjectInput stream
    ) {
        super(position, requirements, forbiddingRequirements, protocol, stream);
    }

    public double getMovementRadius() {
        return movementRadius;
    }

    @Override
    public LocatableEntity getBank() {
        NpcQueryBuilder nqb = Npcs.newQuery().names(getName()).actions(getAction());
        if (movementRadius == 0) {
            nqb.on(getPosition());
        } else {
            nqb.within(new Area.Circular(getPosition(), movementRadius));
        }
        return nqb.results().first();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(getPosition()).append(getName().pattern())
            .append(getAction().pattern()).toHashCode();
    }

    @Override
    public String toString() {
        Coordinate position = getPosition();
        return "NpcBankVertex(name=" + getName() + ", action=" + getAction() + ", radius=" +
            ", x=" + position.getX() + ", y=" + position.getY() + ", plane=" + position.getPlane() +
            ')';
    }

    @Override
    public int getOpcode() {
        return 5;
    }

    @SneakyThrows(IOException.class)
    @Override
    public boolean serialize(ObjectOutput stream) {
        stream.writeDouble(getMovementRadius());
        stream.writeUTF(name.pattern());
        stream.writeInt(name.flags());
        stream.writeUTF(action.pattern());
        stream.writeInt(action.flags());
        return true;
    }

    @SneakyThrows(IOException.class)
    @Override
    public boolean deserialize(int protocol, ObjectInput stream) {
        this.movementRadius = stream.readDouble();
        this.name = Pattern.compile(stream.readUTF(), stream.readInt());
        this.action = Pattern.compile(stream.readUTF(), stream.readInt());
        return true;
    }
}
