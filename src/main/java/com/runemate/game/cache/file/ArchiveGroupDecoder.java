package com.runemate.game.cache.file;

import com.runemate.game.cache.io.*;
import com.runemate.game.cache.util.*;
import java.io.*;
import java.util.*;

public class ArchiveGroupDecoder implements Decoder<Map<Integer, byte[]>> {
    private final int file_count;
    private final int[] file_indices;

    public ArchiveGroupDecoder(final int file_count, final int[] file_indices) {
        this.file_count = file_count;
        this.file_indices = file_indices;
    }

    @Override
    public void decode(Map<Integer, byte[]> groups, byte[] data, int[] keys) {
        try (Js5InputStream stream = new Js5InputStream(Bytes.decompress(data, keys))) {
            if (file_count > 1) {
                int catalog_length = stream.size();
                final int sectors = stream.getBackingArray()[--catalog_length] & 0xFF;
                final int start = catalog_length - 4 * (sectors * file_count);
                stream.seek(start);
                int[] lengths = new int[file_count];
                for (int j = 0; j < sectors; ++j) {
                    int current_length = 0;
                    for (int k = 0; k < file_count; ++k) {
                        current_length += stream.readInt();
                        lengths[k] += current_length;
                    }
                }
                byte[][] entries = new byte[file_count][];
                for (int l = 0; l < file_count; ++l) {
                    entries[l] = new byte[lengths[l]];
                    lengths[l] = 0;
                }
                stream.seek(start);
                int position = 0;
                for (int i = 0; i < sectors; ++i) {
                    int sector_length = 0;
                    for (int n = 0; n < file_count; ++n) {
                        sector_length += stream.readInt();
                        System.arraycopy(stream.getBackingArray(), position, entries[n], lengths[n],
                            sector_length
                        );
                        lengths[n] += sector_length;
                        position += sector_length;
                    }
                }
                for (int i = 0; i < file_count; ++i) {
                    if (file_indices != null) {
                        groups.put(file_indices[i], entries[i]);
                    } else {
                        groups.put(i, entries[i]);
                    }
                }
            } else {
                groups.put(0, stream.getBackingArray());
            }
        } catch (IOException e) {
            throw new IllegalStateException("Unable to decode archive group files.", e);
        }
    }
}
