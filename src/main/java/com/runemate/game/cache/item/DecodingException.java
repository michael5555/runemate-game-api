package com.runemate.game.cache.item;

import java.io.*;
import java.util.*;

public class DecodingException extends IOException {
    public DecodingException(Integer... opcodes) {
        super("Invalid decode format. " + Arrays.toString(opcodes));
    }


    public DecodingException(String message) {
        super(message);
    }
}
